
--[[
Copyright (C) 2016 DBot

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]

--[[
DChat - Experemental impletation of Chatbox that uses HTML.
The idea comes from BeamNG.drive, that uses HTML for entrie HUD.
]]

if SERVER then return end

local CURRENT_FONT = CreateConVar('cl_dchat_font', '1', FCVAR_ARCHIVE, 'Current DChat font. Do not change directly, change it in chat!')
local DISPLAY_TIME = CreateConVar('cl_dchat_time', '1', FCVAR_ARCHIVE, 'Display message time in chat. Do not change directly, change it in chat!')
local FADEOUT_MODE = CreateConVar('cl_dchat_fademode', '1', FCVAR_ARCHIVE, 'Fadeout mode. Do not change directly, change it in chat!')
local DISPLAY_BACKGROUND = CreateConVar('cl_dchat_background', '1', FCVAR_ARCHIVE, 'Display background behind text. Do not change directly, change it in chat!')
local POSTION_X = CreateConVar('cl_dchat_x', '20', FCVAR_ARCHIVE, 'X coordinate')
local POSTION_Y = CreateConVar('cl_dchat_y', tostring(ScrH() - 400), FCVAR_ARCHIVE, 'Y coordinate')

DChat = DChat or {}
DChat.TotalFonts = 1

local HTMLWidth, HTMLHeight = 600, 250
local Width, Height = 830, 350

local function ConsoleMessage(self, msg)
	if not msg then return end
	
	if type(msg) ~= 'string' then msg = tostring(msg) end
	if msg:sub(1, 7) == 'RUNLUA:' then
		CompileString(msg:sub(8), 'DChat')()
		--if not status then
		--	MsgC(Color(0, 200, 0), '[DChat ERROR] ', Color(200, 200, 200), err, '\n')
		--end
	else
		MsgC(Color(0, 200, 0), '[DChat HTML] ', Color(200, 200, 200), msg, '\n')
	end
end

DChat.LastHeartbeat = RealTime()

function DChat.Heartbeat()
	DChat.LastHeartbeat = RealTime()
	if IsValid(DChat.Panel) then
		if DChat.Panel.Frozen and IsValid(DChat.Panel.HTML) then
			DChat.Panel.HTML:SetVisible(true)
		end
		
		DChat.Panel.Frozen = false
	end
end

timer.Create('DChat.Heartbeat', 1, 0, function()
	if DChat.LastHeartbeat + 2 > RealTime() then return end
	if not IsValid(DChat.Panel) then return end
	local pnl = DChat.Panel
	if pnl.Frozen then return end
	pnl.Frozen = true
	DChat.Hide()
	
	if IsValid(DChat.Panel.HTML) then
		DChat.Panel.HTML:SetVisible(false)
	end
end)

local function ThinkFocus(self)
	self:MakePopup()
end

local DragMeta = {
	Paint = function(self, w, h)
		surface.SetDrawColor(200, 200, 200, 150)
		surface.DrawRect(0, 0, w, h)
	end,
	
	OnMousePressed = function(self, key)
		if key ~= MOUSE_LEFT then return end
		self.Trap = true
		self.PressX, self.PressY = gui.MousePos()
	end,
	
	OnMouseReleased = function(self)
		self.Trap = false
	end,
	
	Think = function(self)
		if not self.Trap then return end
		
		if not input.IsMouseDown(MOUSE_LEFT) then
			self.Trap = false
			return
		end
		
		local pnl = self.pnl
		local mx, my = gui.MousePos()
		local ox, oy = self.PressX, self.PressY
		local dx, dy = mx - self.PressX, my - self.PressY
		local wx, wy = pnl:GetPos()
		self.PressX, self.PressY = mx, my
		
		pnl:SetPos(wx + dx, wy + dy)
		RunConsoleCommand('cl_dchat_x', wx + dx)
		RunConsoleCommand('cl_dchat_y', wy + dy)
	end,
}

local RED = Color(200, 100, 100)

surface.CreateFont('DChat.Frozen', {
	size = 48,
	font = 'Roboto',
	weight = 500,
})

local function ChatPaint(self, w, h)
	if not self.Frozen then return end
	
	surface.SetFont('DChat.Frozen')
	local W, H = surface.GetTextSize('DChat is not responding.')
	
	local x, y = 50, 100
	
	surface.SetTextPos(x, y)
	surface.SetTextColor(RED)
	surface.DrawText('DChat is not responding.')
end

local function Check()
	if not DChat.IS_ENABLED then return end
	if IsValid(DChat.Panel) then
		if not IsValid(DChat.Panel.HTML) then
			DChat.Panel:Remove()
		else
			return
		end
	end
	
	local pnl = vgui.Create('EditablePanel')
	DChat.Panel = pnl
	pnl:SetVisible(true)
	pnl:SetSize(Width, Height)
	pnl:SetPos(POSTION_X:GetInt(), POSTION_Y:GetInt())
	--pnl:SetRenderInScreenshots(false)
	pnl.Paint = ChatPaint
	
	local top = pnl:Add('EditablePanel')
	top:SetSize(HTMLWidth, 15)
	top.pnl = pnl
	pnl.top = top
	top:SetCursor('sizeall')
	for k, v in pairs(DragMeta) do
		top[k] = v
	end
	
	local html = pnl:Add('DHTML')
	if not html then DChat.Panel:Remove() timer.Simple(0, Check) return end
	pnl.HTML = html
	html:SetAllowLua(true)
	html:OpenURL('https://dbot.serealia.ca/gchat/')
	html:Refresh(true)
	html.ConsoleMessage = ConsoleMessage
	html:SetPos(0, 15)
	html:SetSize(Width, Height)
	
	local Help = pnl:Add('DButton')
	Help:SetText('If you see this, something wrong happened!\nClick here to rebuild chat panel.\nIf after rebuild nothing happened,\n disable DChat :(.')
	Help:SizeToContents()
	Help:SetPos(0, 0)
	Help.DoClick = function() if IsValid(DChat.Panel) then DChat.Panel:Remove() end Check() end
	Help.Think = ThinkFocus
	
	local w, h = Help:GetSize()
	
	local Help2 = pnl:Add('DButton')
	Help2:SetText('Or you can try to disable DChat!')
	Help2:SizeToContents()
	Help2:SetPos(0, h + 4)
	Help2.DoClick = DChat.Disable
	Help2.Think = ThinkFocus
	
	pnl.HelpPnl2 = Help2
	pnl.HelpPnl = Help
	
	Help:SetMouseInputEnabled(true)
	Help2:SetMouseInputEnabled(true)
		
	Help:SetKeyboardInputEnabled(true)
	Help2:SetKeyboardInputEnabled(true)
	
	DChat.Hide()
	if type(DChat.OldClose) == 'function' then
		pcall(DChat.OldClose)
	end
end

function DChat.RunJS(str)
	Check()
	DChat.Panel.HTML:Call(str)
end

function DChat.OpenSteamProfile(steamid)
	local steamid64 = util.SteamIDTo64(steamid)
	gui.OpenURL('http://steamcommunity.com/profiles/' .. steamid64)
end

function DChat.PlayerAvatar(steamid)
	return {DChatObject = true, type = 'avatar', steamid = steamid}
end

function DChat.CanDisplayAvatar(ply)
	return hook.Run('CanDisplayAvatar', ply) ~= false
end

function DChat.SetFont(id)
	DChat.RunJS('SetFont(' .. (id - 1) .. ');')
end

function DChat.FontObj(font)
	return {DChatObject = true, type = 'font_str', font = font}
end

function DChat.InputToEnd()
	DChat.RunJS("document.getElementById('input').selectionStart = document.getElementById('input').value.length")
end

function DChat.SetInputField(str)
	DChat.RunJS("$('#input').val(" .. ('%q'):format(str) .. ")")
	DChat.InputToEnd()
end

function DChat.LoadSettings()
	DChat.RunJS('SetFont(' .. (CURRENT_FONT:GetInt() - 1) .. ');')
	DChat.RunJS('InternalSetVar("cl_dchat_time", ' .. (DISPLAY_TIME:GetBool() and 'true' or 'false') .. ');')
	DChat.RunJS('InternalSetVar("cl_dchat_fademode", ' .. (FADEOUT_MODE:GetBool() and 'true' or 'false') .. ');')
	DChat.RunJS('InternalSetVar("cl_dchat_background", ' .. (DISPLAY_BACKGROUND:GetBool() and 'true' or 'false') .. ');')
	DChat.RunJS('PopulateSettings();')
	
	if IsValid(DChat.Panel.HelpPnl2) then
		DChat.Panel.HelpPnl2:Remove()
	end
	
	if IsValid(DChat.Panel.HelpPnl) then
		DChat.Panel.HelpPnl:Remove()
	end
	
	DChat.Panel.HTML:RequestFocus()
end

function DChat.AddText(...)
	local js = ''
	
	for k, v in ipairs{...} do
		if type(v) == 'table' then
			if IsColor(v) then
				js = js .. (', Color(%s, %s, %s, %s)'):format(v.r, v.g, v.b, v.a)
			elseif v.DChatObject then
				if v.type == 'avatar' then
					js = js .. (', Avatar("%s")'):format(v.steamid)
				end
			end
			
			continue
		end
		
		local type = type(v)
		
		if type == 'Player' then
			if DChat.CanDisplayAvatar(v) then
				js = js .. (', Avatar("%s")'):format(v.steamid)
			end
			
			local tcolor = team.GetColor(v:Team())
			js = js .. (', Color(%s, %s, %s, %s)'):format(tcolor.r, tcolor.g, tcolor.b, tcolor.a) .. (', %q'):format(v:Nick())
			continue
		end
		
		if type ~= 'string' then continue end
		
		local split = string.Explode(' ', v)
		
		for k, v in ipairs(split) do
			local isUrl = false
			local isSteamID = false
			
			v:gsub('https?://(.*)', function() isUrl = true end)
			v:gsub('^STEAM_[0-9]:[0-9]:[0-9]+$', function() isSteamID = true end)
			
			if isUrl then
				js = js .. (', ChatURL(%q)'):format(v)
			elseif isSteamID then
				js = js .. (', %q, Avatar(%q)'):format(v, v)
			else
				js = js .. (', %q'):format(v)
			end
		end
	end
	
	DChat.RunJS('AddText([' .. js:sub(3) .. ']);')
end

function DChat.Open()
	if IsValid(DChat.Panel) and DChat.Panel.Frozen then
		DChat.Hide()
		return DChat.Panel
	end
	
	Check()
	
	DChat.Panel.HTML:Call('ShowChat(); SetHostName("' .. GetHostName() .. '");')
	DChat.Panel:MakePopup()
	DChat.Panel:SetMouseInputEnabled(true)
	DChat.Panel:SetKeyboardInputEnabled(true)
	DChat.Panel.top:SetVisible(true)
	DChat.Panel.top:SetMouseInputEnabled(true)
	DChat.Panel.top:SetKeyboardInputEnabled(true)
	DChat.Panel:SetVisible(true)
	DChat.Panel.IsOpen = true
	
	if not IsValid(DChat.Panel.HelpPnl) then
		DChat.Panel.HTML:RequestFocus()
	else
		DChat.Panel.HelpPnl:RequestFocus()
		DChat.Panel.HTML:KillFocus()
	end
	
	return DChat.Panel
end

function DChat.Hide()
	Check()
	DChat.Panel.HTML:Call('HideChat(true);')
	DChat.Panel:KillFocus()
	DChat.Panel:SetMouseInputEnabled(false)
	DChat.Panel:SetKeyboardInputEnabled(false)
	DChat.Panel.top:SetVisible(false)
	DChat.Panel.top:SetMouseInputEnabled(false)
	DChat.Panel.top:SetKeyboardInputEnabled(false)
	DChat.Panel.top:KillFocus()
	DChat.Panel.HTML:KillFocus()
	DChat.Panel:SetVisible(true)
	DChat.Panel.IsOpen = false
	
	hook.Run('FinishChat')
	
	return DChat.Panel
end
DChat.Close = DChat.Hide

function DChat.SetIsTeamChat(bool)
	DChat.RunJS('IsTeamChat = ' .. tostring(bool) .. ';')
end

function DChat.GetPos()
	Check()
	return DChat.Panel:GetPos()
end

local REAL_WIDTH = 640

function DChat.GetSize()
	Check()
	local w, h = DChat.Panel:GetSize()
	return REAL_WIDTH, h
end

function DChat.FormatString(...)
	local reply = {}
	
	for k, v in ipairs{...} do
		if type(v) == 'table' then
			if IsColor(v) then
				table.insert(reply, v)
			elseif v.DChatObject then	
				table.insert(reply, '(DChat Object)')
			end
			
			continue
		end
		
		local type = type(v)
		
		if type == 'Player' then
			table.insert(reply, team.GetColor(v:Team()))
			table.insert(reply, v:Nick())
			continue
		end
		
		if type ~= 'string' then continue end
		
		table.insert(reply, v)
	end
	
	return reply
end

local function StartChat(isTeam)
	local x, y = DChat.GetPos()
	local w, h = DChat.GetSize()
	gui.SetMousePos(x + 30, y + h - 60)
	DChat.Open()
	DChat.SetIsTeamChat(isTeam)
	
	hook.Run('StartChat', isTeam)
end

local Gray = Color(200, 200, 200)

local function ChatText(index, name, text, type)
	if type == 'joinleave' then return true end
	chat.AddText(text)
	return true
end

local function AddText(...)
	DChat.AddText(...)
	MsgC(Gray, unpack(DChat.FormatString(...)))
	MsgC('\n')
end

local function Think()
	DChat.NoHookReply = true
	
	if IsValid(DChat.Panel) and not DChat.Panel.IsOpen then
		local can = hook.Run('HUDShouldDraw', 'CHudChat') == false or
			hook.Run('HUDShouldDraw', 'CHudGMod') == false
		
		if can then
			DChat.Panel:SetVisible(false)
		else
			DChat.Panel:SetVisible(true)
		end
	end
	
	DChat.NoHookReply = false
	
	local keyDown = input.IsKeyDown(KEY_SLASH)
	if not keyDown then return end
	local panel = vgui.GetKeyboardFocus()
	if panel then return end
	
	StartChat(false)
	DChat.SetInputField('/')
end

local function HUDShouldDraw(str)
	if DChat.NoHookReply then return end
	if str == 'CHudChat' then return false end
end

local function PlayerBindPress(ply, bind, status)
	if not status then return end
	
	if bind == 'messagemode' then
		StartChat(false)
		return true
	elseif bind == 'messagemode2' then
		StartChat(true)
		return true
	end
end

function DChat.Init()
	if DChat.IS_ENABLED then return end
	DChat.IS_ENABLED = true
	DChat.OldClose = DChat.OldClose or chat.Close
	DChat.OldOpen = DChat.OldOpen or chat.Open
	DChat.OldGetChatBoxPos = DChat.OldGetChatBoxPos or chat.GetChatBoxPos
	DChat.OldGetChatBoxSize = DChat.OldGetChatBoxSize or chat.GetChatBoxSize
	DChat.OldAddText = DChat.OldAddText or chat.AddText

	chat.Open = DChat.Open
	chat.Close = DChat.Close
	chat.GetChatBoxPos = DChat.GetPos
	chat.GetChatBoxSize = DChat.GetSize
	chat.AddText = AddText
	
	hook.Add('Think', 'DChat', Think)
	hook.Add('ChatText', 'DChat', ChatText)
	hook.Add('HUDShouldDraw', 'DChat', HUDShouldDraw)
	hook.Add('PlayerBindPress', 'DChat', PlayerBindPress)
end

function DChat.Disable()
	if not DChat.IS_ENABLED then return end
	DChat.IS_ENABLED = false
	chat.Close = DChat.OldClose
	chat.Open = DChat.OldOpen
	chat.GetChatBoxPos = DChat.OldGetChatBoxPos
	chat.GetChatBoxSize = DChat.OldGetChatBoxSize
	chat.AddText = DChat.OldAddText
	
	DChat.OldClose = nil
	DChat.OldOpen = nil
	DChat.OldGetChatBoxPos = nil
	DChat.OldGetChatBoxSize = nil
	DChat.OldAddText = nil
	
	hook.Remove('Think', 'DChat')
	hook.Remove('ChatText', 'DChat')
	
	if IsValid(DChat.Panel) then
		DChat.Panel:Remove()
	end
	
	chat.AddText(Color(0, 200, 0), '[DChat] ', Color(200, 200, 200), 'Goodnight. To enable DChat again, console command cl_dchat_enable.')
end

if atlaschat and not DChat.AtlasFixed then
	hook.Remove('HUDShouldDraw', 'atlaschat.HUDShouldDraw')
	hook.Remove('PlayerBindPress', 'atlaschat.PlayerBindPress')
	hook.Remove('player_disconnect', 'atlaschat.DisconnectMessage')
	hook.Remove('ChatText', 'atlaschat.ChatText')
	hook.Remove('Think', 'atlaschat.TextSelection')
	
	--It doesn't keep the old one
	chat.Open = function() end
	chat.Close = function() end
	chat.AddText = function() end
	
	if atlaschat.theme and atlaschat.theme.GetValue then
		local pnl = atlaschat.theme.GetValue('panel')
		if IsValid(pnl) then
			pnl:Remove()
		end
	end
	
	DChat.AtlasFixed = true
end

concommand.Add('cl_dchat_disable', function()
	if not DChat.IS_ENABLED then
		MsgC(Color(0, 200, 0), '[DChat] ', Color(200, 200, 200), 'Already disabled!\n')
		return
	end
	
	DChat.Disable()
end)

concommand.Add('cl_dchat_enable', function()
	if DChat.IS_ENABLED then
		MsgC(Color(0, 200, 0), '[DChat] ', Color(200, 200, 200), 'Already enabled!\n')
		return
	end
	
	DChat.Init()
end)

concommand.Add('cl_dchat_rebuild', function()
	if IsValid(DChat.Panel) then
		DChat.Panel:Remove()
	end
	
	Check()
end)

concommand.Add('cl_dchat_reload', function()
	DChat.Disable()
	DChat.Init()
end)

function DChat.Reload()
	DChat.Disable()
	DChat.Init()
end

DChat.Init()
